plugin.tx_hive_thm_jq {
    settings {
        production {
            includePath {
                public = EXT:hive_thm_jq/Resources/Public/
                private = EXT:hive_thm_jq/Resources/Private/
                frontend {
                    public = typo3conf/ext/hive_thm_jq/Resources/Public/
                }

            }
        }
    }
}